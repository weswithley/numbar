/******/ (function(modules) { // webpackBootstrap
/******/ 	function hotDisposeChunk(chunkId) {
/******/ 		delete installedChunks[chunkId];
/******/ 	}
/******/ 	var parentHotUpdateCallback = window["webpackHotUpdate"];
/******/ 	window["webpackHotUpdate"] = 
/******/ 	function webpackHotUpdateCallback(chunkId, moreModules) { // eslint-disable-line no-unused-vars
/******/ 		hotAddUpdateChunk(chunkId, moreModules);
/******/ 		if(parentHotUpdateCallback) parentHotUpdateCallback(chunkId, moreModules);
/******/ 	} ;
/******/ 	
/******/ 	function hotDownloadUpdateChunk(chunkId) { // eslint-disable-line no-unused-vars
/******/ 		var head = document.getElementsByTagName("head")[0];
/******/ 		var script = document.createElement("script");
/******/ 		script.type = "text/javascript";
/******/ 		script.charset = "utf-8";
/******/ 		script.src = __webpack_require__.p + "" + chunkId + "." + hotCurrentHash + ".hot-update.js";
/******/ 		;
/******/ 		head.appendChild(script);
/******/ 	}
/******/ 	
/******/ 	function hotDownloadManifest(requestTimeout) { // eslint-disable-line no-unused-vars
/******/ 		requestTimeout = requestTimeout || 10000;
/******/ 		return new Promise(function(resolve, reject) {
/******/ 			if(typeof XMLHttpRequest === "undefined")
/******/ 				return reject(new Error("No browser support"));
/******/ 			try {
/******/ 				var request = new XMLHttpRequest();
/******/ 				var requestPath = __webpack_require__.p + "" + hotCurrentHash + ".hot-update.json";
/******/ 				request.open("GET", requestPath, true);
/******/ 				request.timeout = requestTimeout;
/******/ 				request.send(null);
/******/ 			} catch(err) {
/******/ 				return reject(err);
/******/ 			}
/******/ 			request.onreadystatechange = function() {
/******/ 				if(request.readyState !== 4) return;
/******/ 				if(request.status === 0) {
/******/ 					// timeout
/******/ 					reject(new Error("Manifest request to " + requestPath + " timed out."));
/******/ 				} else if(request.status === 404) {
/******/ 					// no update available
/******/ 					resolve();
/******/ 				} else if(request.status !== 200 && request.status !== 304) {
/******/ 					// other failure
/******/ 					reject(new Error("Manifest request to " + requestPath + " failed."));
/******/ 				} else {
/******/ 					// success
/******/ 					try {
/******/ 						var update = JSON.parse(request.responseText);
/******/ 					} catch(e) {
/******/ 						reject(e);
/******/ 						return;
/******/ 					}
/******/ 					resolve(update);
/******/ 				}
/******/ 			};
/******/ 		});
/******/ 	}
/******/
/******/ 	
/******/ 	
/******/ 	var hotApplyOnUpdate = true;
/******/ 	var hotCurrentHash = "37979b7b0431f51cc0d9"; // eslint-disable-line no-unused-vars
/******/ 	var hotRequestTimeout = 10000;
/******/ 	var hotCurrentModuleData = {};
/******/ 	var hotCurrentChildModule; // eslint-disable-line no-unused-vars
/******/ 	var hotCurrentParents = []; // eslint-disable-line no-unused-vars
/******/ 	var hotCurrentParentsTemp = []; // eslint-disable-line no-unused-vars
/******/ 	
/******/ 	function hotCreateRequire(moduleId) { // eslint-disable-line no-unused-vars
/******/ 		var me = installedModules[moduleId];
/******/ 		if(!me) return __webpack_require__;
/******/ 		var fn = function(request) {
/******/ 			if(me.hot.active) {
/******/ 				if(installedModules[request]) {
/******/ 					if(installedModules[request].parents.indexOf(moduleId) < 0)
/******/ 						installedModules[request].parents.push(moduleId);
/******/ 				} else {
/******/ 					hotCurrentParents = [moduleId];
/******/ 					hotCurrentChildModule = request;
/******/ 				}
/******/ 				if(me.children.indexOf(request) < 0)
/******/ 					me.children.push(request);
/******/ 			} else {
/******/ 				console.warn("[HMR] unexpected require(" + request + ") from disposed module " + moduleId);
/******/ 				hotCurrentParents = [];
/******/ 			}
/******/ 			return __webpack_require__(request);
/******/ 		};
/******/ 		var ObjectFactory = function ObjectFactory(name) {
/******/ 			return {
/******/ 				configurable: true,
/******/ 				enumerable: true,
/******/ 				get: function() {
/******/ 					return __webpack_require__[name];
/******/ 				},
/******/ 				set: function(value) {
/******/ 					__webpack_require__[name] = value;
/******/ 				}
/******/ 			};
/******/ 		};
/******/ 		for(var name in __webpack_require__) {
/******/ 			if(Object.prototype.hasOwnProperty.call(__webpack_require__, name) && name !== "e") {
/******/ 				Object.defineProperty(fn, name, ObjectFactory(name));
/******/ 			}
/******/ 		}
/******/ 		fn.e = function(chunkId) {
/******/ 			if(hotStatus === "ready")
/******/ 				hotSetStatus("prepare");
/******/ 			hotChunksLoading++;
/******/ 			return __webpack_require__.e(chunkId).then(finishChunkLoading, function(err) {
/******/ 				finishChunkLoading();
/******/ 				throw err;
/******/ 			});
/******/ 	
/******/ 			function finishChunkLoading() {
/******/ 				hotChunksLoading--;
/******/ 				if(hotStatus === "prepare") {
/******/ 					if(!hotWaitingFilesMap[chunkId]) {
/******/ 						hotEnsureUpdateChunk(chunkId);
/******/ 					}
/******/ 					if(hotChunksLoading === 0 && hotWaitingFiles === 0) {
/******/ 						hotUpdateDownloaded();
/******/ 					}
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 		return fn;
/******/ 	}
/******/ 	
/******/ 	function hotCreateModule(moduleId) { // eslint-disable-line no-unused-vars
/******/ 		var hot = {
/******/ 			// private stuff
/******/ 			_acceptedDependencies: {},
/******/ 			_declinedDependencies: {},
/******/ 			_selfAccepted: false,
/******/ 			_selfDeclined: false,
/******/ 			_disposeHandlers: [],
/******/ 			_main: hotCurrentChildModule !== moduleId,
/******/ 	
/******/ 			// Module API
/******/ 			active: true,
/******/ 			accept: function(dep, callback) {
/******/ 				if(typeof dep === "undefined")
/******/ 					hot._selfAccepted = true;
/******/ 				else if(typeof dep === "function")
/******/ 					hot._selfAccepted = dep;
/******/ 				else if(typeof dep === "object")
/******/ 					for(var i = 0; i < dep.length; i++)
/******/ 						hot._acceptedDependencies[dep[i]] = callback || function() {};
/******/ 				else
/******/ 					hot._acceptedDependencies[dep] = callback || function() {};
/******/ 			},
/******/ 			decline: function(dep) {
/******/ 				if(typeof dep === "undefined")
/******/ 					hot._selfDeclined = true;
/******/ 				else if(typeof dep === "object")
/******/ 					for(var i = 0; i < dep.length; i++)
/******/ 						hot._declinedDependencies[dep[i]] = true;
/******/ 				else
/******/ 					hot._declinedDependencies[dep] = true;
/******/ 			},
/******/ 			dispose: function(callback) {
/******/ 				hot._disposeHandlers.push(callback);
/******/ 			},
/******/ 			addDisposeHandler: function(callback) {
/******/ 				hot._disposeHandlers.push(callback);
/******/ 			},
/******/ 			removeDisposeHandler: function(callback) {
/******/ 				var idx = hot._disposeHandlers.indexOf(callback);
/******/ 				if(idx >= 0) hot._disposeHandlers.splice(idx, 1);
/******/ 			},
/******/ 	
/******/ 			// Management API
/******/ 			check: hotCheck,
/******/ 			apply: hotApply,
/******/ 			status: function(l) {
/******/ 				if(!l) return hotStatus;
/******/ 				hotStatusHandlers.push(l);
/******/ 			},
/******/ 			addStatusHandler: function(l) {
/******/ 				hotStatusHandlers.push(l);
/******/ 			},
/******/ 			removeStatusHandler: function(l) {
/******/ 				var idx = hotStatusHandlers.indexOf(l);
/******/ 				if(idx >= 0) hotStatusHandlers.splice(idx, 1);
/******/ 			},
/******/ 	
/******/ 			//inherit from previous dispose call
/******/ 			data: hotCurrentModuleData[moduleId]
/******/ 		};
/******/ 		hotCurrentChildModule = undefined;
/******/ 		return hot;
/******/ 	}
/******/ 	
/******/ 	var hotStatusHandlers = [];
/******/ 	var hotStatus = "idle";
/******/ 	
/******/ 	function hotSetStatus(newStatus) {
/******/ 		hotStatus = newStatus;
/******/ 		for(var i = 0; i < hotStatusHandlers.length; i++)
/******/ 			hotStatusHandlers[i].call(null, newStatus);
/******/ 	}
/******/ 	
/******/ 	// while downloading
/******/ 	var hotWaitingFiles = 0;
/******/ 	var hotChunksLoading = 0;
/******/ 	var hotWaitingFilesMap = {};
/******/ 	var hotRequestedFilesMap = {};
/******/ 	var hotAvailableFilesMap = {};
/******/ 	var hotDeferred;
/******/ 	
/******/ 	// The update info
/******/ 	var hotUpdate, hotUpdateNewHash;
/******/ 	
/******/ 	function toModuleId(id) {
/******/ 		var isNumber = (+id) + "" === id;
/******/ 		return isNumber ? +id : id;
/******/ 	}
/******/ 	
/******/ 	function hotCheck(apply) {
/******/ 		if(hotStatus !== "idle") throw new Error("check() is only allowed in idle status");
/******/ 		hotApplyOnUpdate = apply;
/******/ 		hotSetStatus("check");
/******/ 		return hotDownloadManifest(hotRequestTimeout).then(function(update) {
/******/ 			if(!update) {
/******/ 				hotSetStatus("idle");
/******/ 				return null;
/******/ 			}
/******/ 			hotRequestedFilesMap = {};
/******/ 			hotWaitingFilesMap = {};
/******/ 			hotAvailableFilesMap = update.c;
/******/ 			hotUpdateNewHash = update.h;
/******/ 	
/******/ 			hotSetStatus("prepare");
/******/ 			var promise = new Promise(function(resolve, reject) {
/******/ 				hotDeferred = {
/******/ 					resolve: resolve,
/******/ 					reject: reject
/******/ 				};
/******/ 			});
/******/ 			hotUpdate = {};
/******/ 			var chunkId = 0;
/******/ 			{ // eslint-disable-line no-lone-blocks
/******/ 				/*globals chunkId */
/******/ 				hotEnsureUpdateChunk(chunkId);
/******/ 			}
/******/ 			if(hotStatus === "prepare" && hotChunksLoading === 0 && hotWaitingFiles === 0) {
/******/ 				hotUpdateDownloaded();
/******/ 			}
/******/ 			return promise;
/******/ 		});
/******/ 	}
/******/ 	
/******/ 	function hotAddUpdateChunk(chunkId, moreModules) { // eslint-disable-line no-unused-vars
/******/ 		if(!hotAvailableFilesMap[chunkId] || !hotRequestedFilesMap[chunkId])
/******/ 			return;
/******/ 		hotRequestedFilesMap[chunkId] = false;
/******/ 		for(var moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				hotUpdate[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(--hotWaitingFiles === 0 && hotChunksLoading === 0) {
/******/ 			hotUpdateDownloaded();
/******/ 		}
/******/ 	}
/******/ 	
/******/ 	function hotEnsureUpdateChunk(chunkId) {
/******/ 		if(!hotAvailableFilesMap[chunkId]) {
/******/ 			hotWaitingFilesMap[chunkId] = true;
/******/ 		} else {
/******/ 			hotRequestedFilesMap[chunkId] = true;
/******/ 			hotWaitingFiles++;
/******/ 			hotDownloadUpdateChunk(chunkId);
/******/ 		}
/******/ 	}
/******/ 	
/******/ 	function hotUpdateDownloaded() {
/******/ 		hotSetStatus("ready");
/******/ 		var deferred = hotDeferred;
/******/ 		hotDeferred = null;
/******/ 		if(!deferred) return;
/******/ 		if(hotApplyOnUpdate) {
/******/ 			// Wrap deferred object in Promise to mark it as a well-handled Promise to
/******/ 			// avoid triggering uncaught exception warning in Chrome.
/******/ 			// See https://bugs.chromium.org/p/chromium/issues/detail?id=465666
/******/ 			Promise.resolve().then(function() {
/******/ 				return hotApply(hotApplyOnUpdate);
/******/ 			}).then(
/******/ 				function(result) {
/******/ 					deferred.resolve(result);
/******/ 				},
/******/ 				function(err) {
/******/ 					deferred.reject(err);
/******/ 				}
/******/ 			);
/******/ 		} else {
/******/ 			var outdatedModules = [];
/******/ 			for(var id in hotUpdate) {
/******/ 				if(Object.prototype.hasOwnProperty.call(hotUpdate, id)) {
/******/ 					outdatedModules.push(toModuleId(id));
/******/ 				}
/******/ 			}
/******/ 			deferred.resolve(outdatedModules);
/******/ 		}
/******/ 	}
/******/ 	
/******/ 	function hotApply(options) {
/******/ 		if(hotStatus !== "ready") throw new Error("apply() is only allowed in ready status");
/******/ 		options = options || {};
/******/ 	
/******/ 		var cb;
/******/ 		var i;
/******/ 		var j;
/******/ 		var module;
/******/ 		var moduleId;
/******/ 	
/******/ 		function getAffectedStuff(updateModuleId) {
/******/ 			var outdatedModules = [updateModuleId];
/******/ 			var outdatedDependencies = {};
/******/ 	
/******/ 			var queue = outdatedModules.slice().map(function(id) {
/******/ 				return {
/******/ 					chain: [id],
/******/ 					id: id
/******/ 				};
/******/ 			});
/******/ 			while(queue.length > 0) {
/******/ 				var queueItem = queue.pop();
/******/ 				var moduleId = queueItem.id;
/******/ 				var chain = queueItem.chain;
/******/ 				module = installedModules[moduleId];
/******/ 				if(!module || module.hot._selfAccepted)
/******/ 					continue;
/******/ 				if(module.hot._selfDeclined) {
/******/ 					return {
/******/ 						type: "self-declined",
/******/ 						chain: chain,
/******/ 						moduleId: moduleId
/******/ 					};
/******/ 				}
/******/ 				if(module.hot._main) {
/******/ 					return {
/******/ 						type: "unaccepted",
/******/ 						chain: chain,
/******/ 						moduleId: moduleId
/******/ 					};
/******/ 				}
/******/ 				for(var i = 0; i < module.parents.length; i++) {
/******/ 					var parentId = module.parents[i];
/******/ 					var parent = installedModules[parentId];
/******/ 					if(!parent) continue;
/******/ 					if(parent.hot._declinedDependencies[moduleId]) {
/******/ 						return {
/******/ 							type: "declined",
/******/ 							chain: chain.concat([parentId]),
/******/ 							moduleId: moduleId,
/******/ 							parentId: parentId
/******/ 						};
/******/ 					}
/******/ 					if(outdatedModules.indexOf(parentId) >= 0) continue;
/******/ 					if(parent.hot._acceptedDependencies[moduleId]) {
/******/ 						if(!outdatedDependencies[parentId])
/******/ 							outdatedDependencies[parentId] = [];
/******/ 						addAllToSet(outdatedDependencies[parentId], [moduleId]);
/******/ 						continue;
/******/ 					}
/******/ 					delete outdatedDependencies[parentId];
/******/ 					outdatedModules.push(parentId);
/******/ 					queue.push({
/******/ 						chain: chain.concat([parentId]),
/******/ 						id: parentId
/******/ 					});
/******/ 				}
/******/ 			}
/******/ 	
/******/ 			return {
/******/ 				type: "accepted",
/******/ 				moduleId: updateModuleId,
/******/ 				outdatedModules: outdatedModules,
/******/ 				outdatedDependencies: outdatedDependencies
/******/ 			};
/******/ 		}
/******/ 	
/******/ 		function addAllToSet(a, b) {
/******/ 			for(var i = 0; i < b.length; i++) {
/******/ 				var item = b[i];
/******/ 				if(a.indexOf(item) < 0)
/******/ 					a.push(item);
/******/ 			}
/******/ 		}
/******/ 	
/******/ 		// at begin all updates modules are outdated
/******/ 		// the "outdated" status can propagate to parents if they don't accept the children
/******/ 		var outdatedDependencies = {};
/******/ 		var outdatedModules = [];
/******/ 		var appliedUpdate = {};
/******/ 	
/******/ 		var warnUnexpectedRequire = function warnUnexpectedRequire() {
/******/ 			console.warn("[HMR] unexpected require(" + result.moduleId + ") to disposed module");
/******/ 		};
/******/ 	
/******/ 		for(var id in hotUpdate) {
/******/ 			if(Object.prototype.hasOwnProperty.call(hotUpdate, id)) {
/******/ 				moduleId = toModuleId(id);
/******/ 				var result;
/******/ 				if(hotUpdate[id]) {
/******/ 					result = getAffectedStuff(moduleId);
/******/ 				} else {
/******/ 					result = {
/******/ 						type: "disposed",
/******/ 						moduleId: id
/******/ 					};
/******/ 				}
/******/ 				var abortError = false;
/******/ 				var doApply = false;
/******/ 				var doDispose = false;
/******/ 				var chainInfo = "";
/******/ 				if(result.chain) {
/******/ 					chainInfo = "\nUpdate propagation: " + result.chain.join(" -> ");
/******/ 				}
/******/ 				switch(result.type) {
/******/ 					case "self-declined":
/******/ 						if(options.onDeclined)
/******/ 							options.onDeclined(result);
/******/ 						if(!options.ignoreDeclined)
/******/ 							abortError = new Error("Aborted because of self decline: " + result.moduleId + chainInfo);
/******/ 						break;
/******/ 					case "declined":
/******/ 						if(options.onDeclined)
/******/ 							options.onDeclined(result);
/******/ 						if(!options.ignoreDeclined)
/******/ 							abortError = new Error("Aborted because of declined dependency: " + result.moduleId + " in " + result.parentId + chainInfo);
/******/ 						break;
/******/ 					case "unaccepted":
/******/ 						if(options.onUnaccepted)
/******/ 							options.onUnaccepted(result);
/******/ 						if(!options.ignoreUnaccepted)
/******/ 							abortError = new Error("Aborted because " + moduleId + " is not accepted" + chainInfo);
/******/ 						break;
/******/ 					case "accepted":
/******/ 						if(options.onAccepted)
/******/ 							options.onAccepted(result);
/******/ 						doApply = true;
/******/ 						break;
/******/ 					case "disposed":
/******/ 						if(options.onDisposed)
/******/ 							options.onDisposed(result);
/******/ 						doDispose = true;
/******/ 						break;
/******/ 					default:
/******/ 						throw new Error("Unexception type " + result.type);
/******/ 				}
/******/ 				if(abortError) {
/******/ 					hotSetStatus("abort");
/******/ 					return Promise.reject(abortError);
/******/ 				}
/******/ 				if(doApply) {
/******/ 					appliedUpdate[moduleId] = hotUpdate[moduleId];
/******/ 					addAllToSet(outdatedModules, result.outdatedModules);
/******/ 					for(moduleId in result.outdatedDependencies) {
/******/ 						if(Object.prototype.hasOwnProperty.call(result.outdatedDependencies, moduleId)) {
/******/ 							if(!outdatedDependencies[moduleId])
/******/ 								outdatedDependencies[moduleId] = [];
/******/ 							addAllToSet(outdatedDependencies[moduleId], result.outdatedDependencies[moduleId]);
/******/ 						}
/******/ 					}
/******/ 				}
/******/ 				if(doDispose) {
/******/ 					addAllToSet(outdatedModules, [result.moduleId]);
/******/ 					appliedUpdate[moduleId] = warnUnexpectedRequire;
/******/ 				}
/******/ 			}
/******/ 		}
/******/ 	
/******/ 		// Store self accepted outdated modules to require them later by the module system
/******/ 		var outdatedSelfAcceptedModules = [];
/******/ 		for(i = 0; i < outdatedModules.length; i++) {
/******/ 			moduleId = outdatedModules[i];
/******/ 			if(installedModules[moduleId] && installedModules[moduleId].hot._selfAccepted)
/******/ 				outdatedSelfAcceptedModules.push({
/******/ 					module: moduleId,
/******/ 					errorHandler: installedModules[moduleId].hot._selfAccepted
/******/ 				});
/******/ 		}
/******/ 	
/******/ 		// Now in "dispose" phase
/******/ 		hotSetStatus("dispose");
/******/ 		Object.keys(hotAvailableFilesMap).forEach(function(chunkId) {
/******/ 			if(hotAvailableFilesMap[chunkId] === false) {
/******/ 				hotDisposeChunk(chunkId);
/******/ 			}
/******/ 		});
/******/ 	
/******/ 		var idx;
/******/ 		var queue = outdatedModules.slice();
/******/ 		while(queue.length > 0) {
/******/ 			moduleId = queue.pop();
/******/ 			module = installedModules[moduleId];
/******/ 			if(!module) continue;
/******/ 	
/******/ 			var data = {};
/******/ 	
/******/ 			// Call dispose handlers
/******/ 			var disposeHandlers = module.hot._disposeHandlers;
/******/ 			for(j = 0; j < disposeHandlers.length; j++) {
/******/ 				cb = disposeHandlers[j];
/******/ 				cb(data);
/******/ 			}
/******/ 			hotCurrentModuleData[moduleId] = data;
/******/ 	
/******/ 			// disable module (this disables requires from this module)
/******/ 			module.hot.active = false;
/******/ 	
/******/ 			// remove module from cache
/******/ 			delete installedModules[moduleId];
/******/ 	
/******/ 			// when disposing there is no need to call dispose handler
/******/ 			delete outdatedDependencies[moduleId];
/******/ 	
/******/ 			// remove "parents" references from all children
/******/ 			for(j = 0; j < module.children.length; j++) {
/******/ 				var child = installedModules[module.children[j]];
/******/ 				if(!child) continue;
/******/ 				idx = child.parents.indexOf(moduleId);
/******/ 				if(idx >= 0) {
/******/ 					child.parents.splice(idx, 1);
/******/ 				}
/******/ 			}
/******/ 		}
/******/ 	
/******/ 		// remove outdated dependency from module children
/******/ 		var dependency;
/******/ 		var moduleOutdatedDependencies;
/******/ 		for(moduleId in outdatedDependencies) {
/******/ 			if(Object.prototype.hasOwnProperty.call(outdatedDependencies, moduleId)) {
/******/ 				module = installedModules[moduleId];
/******/ 				if(module) {
/******/ 					moduleOutdatedDependencies = outdatedDependencies[moduleId];
/******/ 					for(j = 0; j < moduleOutdatedDependencies.length; j++) {
/******/ 						dependency = moduleOutdatedDependencies[j];
/******/ 						idx = module.children.indexOf(dependency);
/******/ 						if(idx >= 0) module.children.splice(idx, 1);
/******/ 					}
/******/ 				}
/******/ 			}
/******/ 		}
/******/ 	
/******/ 		// Not in "apply" phase
/******/ 		hotSetStatus("apply");
/******/ 	
/******/ 		hotCurrentHash = hotUpdateNewHash;
/******/ 	
/******/ 		// insert new code
/******/ 		for(moduleId in appliedUpdate) {
/******/ 			if(Object.prototype.hasOwnProperty.call(appliedUpdate, moduleId)) {
/******/ 				modules[moduleId] = appliedUpdate[moduleId];
/******/ 			}
/******/ 		}
/******/ 	
/******/ 		// call accept handlers
/******/ 		var error = null;
/******/ 		for(moduleId in outdatedDependencies) {
/******/ 			if(Object.prototype.hasOwnProperty.call(outdatedDependencies, moduleId)) {
/******/ 				module = installedModules[moduleId];
/******/ 				if(module) {
/******/ 					moduleOutdatedDependencies = outdatedDependencies[moduleId];
/******/ 					var callbacks = [];
/******/ 					for(i = 0; i < moduleOutdatedDependencies.length; i++) {
/******/ 						dependency = moduleOutdatedDependencies[i];
/******/ 						cb = module.hot._acceptedDependencies[dependency];
/******/ 						if(cb) {
/******/ 							if(callbacks.indexOf(cb) >= 0) continue;
/******/ 							callbacks.push(cb);
/******/ 						}
/******/ 					}
/******/ 					for(i = 0; i < callbacks.length; i++) {
/******/ 						cb = callbacks[i];
/******/ 						try {
/******/ 							cb(moduleOutdatedDependencies);
/******/ 						} catch(err) {
/******/ 							if(options.onErrored) {
/******/ 								options.onErrored({
/******/ 									type: "accept-errored",
/******/ 									moduleId: moduleId,
/******/ 									dependencyId: moduleOutdatedDependencies[i],
/******/ 									error: err
/******/ 								});
/******/ 							}
/******/ 							if(!options.ignoreErrored) {
/******/ 								if(!error)
/******/ 									error = err;
/******/ 							}
/******/ 						}
/******/ 					}
/******/ 				}
/******/ 			}
/******/ 		}
/******/ 	
/******/ 		// Load self accepted modules
/******/ 		for(i = 0; i < outdatedSelfAcceptedModules.length; i++) {
/******/ 			var item = outdatedSelfAcceptedModules[i];
/******/ 			moduleId = item.module;
/******/ 			hotCurrentParents = [moduleId];
/******/ 			try {
/******/ 				__webpack_require__(moduleId);
/******/ 			} catch(err) {
/******/ 				if(typeof item.errorHandler === "function") {
/******/ 					try {
/******/ 						item.errorHandler(err);
/******/ 					} catch(err2) {
/******/ 						if(options.onErrored) {
/******/ 							options.onErrored({
/******/ 								type: "self-accept-error-handler-errored",
/******/ 								moduleId: moduleId,
/******/ 								error: err2,
/******/ 								orginalError: err, // TODO remove in webpack 4
/******/ 								originalError: err
/******/ 							});
/******/ 						}
/******/ 						if(!options.ignoreErrored) {
/******/ 							if(!error)
/******/ 								error = err2;
/******/ 						}
/******/ 						if(!error)
/******/ 							error = err;
/******/ 					}
/******/ 				} else {
/******/ 					if(options.onErrored) {
/******/ 						options.onErrored({
/******/ 							type: "self-accept-errored",
/******/ 							moduleId: moduleId,
/******/ 							error: err
/******/ 						});
/******/ 					}
/******/ 					if(!options.ignoreErrored) {
/******/ 						if(!error)
/******/ 							error = err;
/******/ 					}
/******/ 				}
/******/ 			}
/******/ 		}
/******/ 	
/******/ 		// handle errors in accept handlers and self accepted module load
/******/ 		if(error) {
/******/ 			hotSetStatus("fail");
/******/ 			return Promise.reject(error);
/******/ 		}
/******/ 	
/******/ 		hotSetStatus("idle");
/******/ 		return new Promise(function(resolve) {
/******/ 			resolve(outdatedModules);
/******/ 		});
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {},
/******/ 			hot: hotCreateModule(moduleId),
/******/ 			parents: (hotCurrentParentsTemp = hotCurrentParents, hotCurrentParents = [], hotCurrentParentsTemp),
/******/ 			children: []
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, hotCreateRequire(moduleId));
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// __webpack_hash__
/******/ 	__webpack_require__.h = function() { return hotCurrentHash; };
/******/
/******/ 	// Load entry module and return exports
/******/ 	return hotCreateRequire(0)(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

__webpack_require__(2);

var _module = __webpack_require__(3);

var root = function (root) {
	if ((typeof root === 'undefined' ? 'undefined' : _typeof(root)) === 'object' && (root.self === root || root.global === global) && root) {
		return root;
	}
}(self || global || {});

var $ = function ($) {
	if (typeof $ === 'function') {
		return $;
	} else {
		throw 'You must import jQuery';
	}
}(root.jQuery);

$.fn[_module.ModuleName] = function () {
	var module = void 0;
	var args = Array.prototype.slice.call(arguments, 0);
	var method = args[0];
	var options = args.slice(1).length <= 0 ? undefined : args.slice(1, args.length);;
	var isReturnMethod = this.length === 1 && typeof method === 'string' && _module.ModuleReturns.indexOf(method) !== -1;
	var methodRunner = function methodRunner(method, options, uesReturn) {
		var $this = $(this);
		var module = $this.data(_module.ModuleName);
		if (!!module) {
			if (typeof method == 'string' && !uesReturn) {
				module[method].apply(module, options);
			} else if (typeof method == 'string' && !!uesReturn) {
				return module[method].apply(module, options);
			} else {
				throw 'unsupported options!';
			}
		} else {
			throw 'You must run first this plugin!';
		}
	};
	if (isReturnMethod) {
		return methodRunner.call(this, method, options, isReturnMethod);
	} else {
		return this.each(function () {
			var $this = $(this);
			var module = $this.data(_module.ModuleName);
			var opts = null;
			if (!!module) {
				methodRunner.call(this, method, options);
			} else {
				opts = $.extend(true, {}, _module.ModuleDefaults, (typeof method === 'undefined' ? 'undefined' : _typeof(method)) === 'object' && method, (typeof options === 'undefined' ? 'undefined' : _typeof(options)) === 'object' && options);
				module = new _module.Module(this, opts);
				$this.data(_module.ModuleName, module);
				module.init();
			}
		});
	}
};
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(1)))

/***/ }),
/* 1 */
/***/ (function(module, exports) {

var g;

// This works in non-strict mode
g = (function() {
	return this;
})();

try {
	// This works if eval is allowed (see CSP)
	g = g || Function("return this")() || (1,eval)("this");
} catch(e) {
	// This works if the window reference is available
	if(typeof window === "object")
		g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;


/***/ }),
/* 2 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
		value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var ModuleName = 'numbar';
var ModuleDefaults = {
		min: null, // string / number.
		max: null, // string / number.
		disable: false, // boolean.
		mode: 'number', // string.
		reInit: function reInit() {}, // function.
		setValues: function setValues() {}, // function.
		change: function change() {} // function.
};
var ModuleReturns = ['output', 'methods'];

var Module = function () {
		function Module(ele, options) {
				_classCallCheck(this, Module);

				this.ele = ele;
				this.$ele = $(ele);
				this.option = options;
				this.barWrap = '<div class="slider_range_wrap">\n\t\t\t\t\t\t\t<div class="slider_btn_before slider_btn"><span></span></div>\n\t\t\t\t\t\t\t<div class="slider_btn_after slider_btn"><span></span></div>\n\t\t\t\t\t\t\t<div class="slider_range_red slider_bg"></div>\n\t\t\t\t\t\t\t<div class="slider_range_grey slider_bg"></div>\n\t\t\t\t\t\t</div>';
				this.$beforeBtn;

				this.$afterBtn;

				this.$greyBar;
				this.greyBarW = 0;

				this.$redBar;

				this.valueArr = [];

				this.disable = this.option.disable;

				this.changeCallBack = this.option.change;

				this.mode = this.option.mode || ModuleDefaults.mode;

				this.min = 0;
				this.max = 100;

				// this.isSetValue = false;
				this.setValueTempArr = [];

				this.mouseClickXnum = 0;

				this.currentBtn; // 目前點擊的btn.
		}

		_createClass(Module, [{
				key: 'convertNumToPercentage',
				value: function convertNumToPercentage(percentNum) {
						// 把輸入number, 轉為greyBarW的百分比.
						var percentValue = Math.floor(percentNum / this.greyBarW * 100);
						return percentValue;
				}
		}, {
				key: 'convertPercentageToNum',
				value: function convertPercentageToNum(percentNum) {
						// 把輸入百分比, 轉為greyBarW的百分比.
						if (percentNum > 100) {
								percentNum = 100;
						} else if (percentNum < 0) {
								percentNum = 0;
						}
						var numValue = this.greyBarW * (percentNum / 100);

						return numValue;
				}
		}, {
				key: 'convertTimeToPecentage',
				value: function convertTimeToPecentage(timeString) {

						// 把輸入時間字串, 轉為greyBarW的百分比.
						var timeStringSplitArr = timeString.split(':');
						// 時
						var hourValue = Number(timeStringSplitArr[0]) * 60;
						// 分
						var minsValue = 0; //分不用做運算處理, 所以直接為0.
						var totalValue = Math.floor((hourValue + minsValue) / 1440 * 100);

						if (totalValue > 100) {
								totalValue = 100;
						} else if (totalValue < 0) {
								totalValue = 0;
						}

						return totalValue;
				}
		}, {
				key: 'convertPecentageToTime',
				value: function convertPecentageToTime(percentNum) {
						// 把輸入的百分比, 轉為時間字串.
						var percentValue = Math.round(1440 * (percentNum / 100) / 60); // 改為四捨五入.
						var percentString = '';
						if (percentValue >= 10) {
								percentString = percentValue.toString() + ':00';
						} else {
								percentString = '0' + percentValue.toString() + ':00';
						}
						return percentString;
				}
		}, {
				key: 'changeCallBack',
				value: function changeCallBack(arr) {
						// 數值change時的callback.
				}
		}, {
				key: 'setMinMaxInit',
				value: function setMinMaxInit() {
						if (this.option.min) {
								if (typeof this.option.min == 'string' || this.mode == 'time') {
										// 為時間模式
										this.min = this.convertTimeToPecentage(this.option.min);
										this.valueArr[0] = this.option.min;
								} else {
										// 為數字模式
										this.min = this.option.min;
										this.valueArr[0] = this.option.min;
								}
						} else {
								if (this.mode == 'time') {
										// 為時間模式
										this.valueArr[0] = this.convertPecentageToTime(this.min);
								} else {
										// 為數字模式
										this.valueArr[0] = this.min;
								}
						}

						if (this.option.max) {
								if (typeof this.option.max == 'string' || this.mode == 'time') {
										// 為時間模式
										this.max = this.convertTimeToPecentage(this.option.max);
										this.valueArr[1] = this.option.max;
								} else {
										// 為數字模式
										this.max = this.option.max;
										this.valueArr[1] = this.option.max;
								}
						} else {
								if (this.mode == 'time') {
										// 為時間模式
										this.valueArr[1] = this.convertPecentageToTime(this.max);
								} else {
										// 為數字模式
										this.valueArr[1] = this.max = this.max;
								}
						}
				}
		}, {
				key: 'checkNearestBtn',
				value: function checkNearestBtn(num) {

						var mouseToBeforeBtn = num - (this.$beforeBtn.offset().left + 10);
						if (mouseToBeforeBtn < 0) mouseToBeforeBtn = mouseToBeforeBtn * -1;

						var mouseToAfterBtn = this.$afterBtn.offset().left + 10 - num;
						if (mouseToAfterBtn < 0) mouseToAfterBtn = mouseToAfterBtn * -1;

						if (mouseToBeforeBtn < mouseToAfterBtn) {
								// 靠近before btn.
								return 'before';
						} else if (mouseToBeforeBtn > mouseToAfterBtn) {
								// 靠近after btn.
								return 'after';
						} else {

								if (this.currentBtn == this.$beforeBtn) {
										return 'before';
								} else if (this.currentBtn == this.$afterBtn) {
										return 'after';
								} else {
										console.log('checkNearestBtn get error btn : ' + this.currentBtn);
								}
						}
				}
		}, {
				key: 'setBtnPosition',
				value: function setBtnPosition(typeOfBtn, num) {

						if (typeOfBtn == 'before') {

								// before.
								var tempMin = this.convertPercentageToNum(this.min);

								//set btn z-index;
								this.$beforeBtn.css('z-index', 9);
								this.$afterBtn.css('z-index', 5);

								this.currentBtn = this.$beforeBtn;

								//before btn.
								this.$beforeBtn.children('span').addClass('active');

								// 限制區域
								if (num < this.$greyBar.offset().left + tempMin) num = this.$greyBar.offset().left + tempMin;else if (num > this.$afterBtn.offset().left + 10) num = this.$afterBtn.offset().left + 10;

								// 按鈕跟隨滑鼠.
								this.setBtnFollowMouse(this.$beforeBtn, num);

								if (this.mode == 'time') {
										this.valueArr[0] = this.convertPecentageToTime(this.convertNumToPercentage(num - this.$greyBar.offset().left));
								} else {
										this.valueArr[0] = this.convertNumToPercentage(num - this.$greyBar.offset().left);
								}

								// redBar的縮放
								this.setRedBarW();

								// 把目前變換的value array存進callback.
								this.changeCallBack(this.valueArr);
						} else if (typeOfBtn == 'after') {

								// after.
								var tempMax = this.convertPercentageToNum(this.max);

								//set btn z-index;
								this.$beforeBtn.css('z-index', 5);
								this.$afterBtn.css('z-index', 9);

								this.currentBtn = this.$afterBtn;

								//after btn.
								this.$afterBtn.children('span').addClass('active');

								// 限制區域
								if (num > this.$greyBar.offset().left + tempMax) num = this.$greyBar.offset().left + tempMax;else if (num < this.$beforeBtn.offset().left + 10) num = this.$beforeBtn.offset().left + 10;

								// 按鈕跟隨滑鼠.
								this.setBtnFollowMouse(this.$afterBtn, num);

								if (this.mode == 'time') {
										this.valueArr[1] = this.convertPecentageToTime(this.convertNumToPercentage(num - this.$greyBar.offset().left));
								} else {
										this.valueArr[1] = this.convertNumToPercentage(num - this.$greyBar.offset().left);
								}

								// redBar的縮放
								this.setRedBarW();

								// 把目前變換的value array存進callback.
								this.changeCallBack(this.valueArr);
						} else {
								console.log('setBtnPosition()傳入參數格式錯誤 : ' + typeOfBtn);
						}
				}
		}, {
				key: 'setMouseDown',
				value: function setMouseDown() {
						var _this = this;

						this.$beforeBtn.on('mousedown touchstart', function (e) {

								e.preventDefault();

								// 前面按鈕處置.
								_this.$ele.on('mousemove touchmove', function (e) {

										e.preventDefault();

										// mouse X.
										var num = e.clientX || e.originalEvent.touches[0].pageX;

										// 設定按鈕位置.
										_this.setBtnPosition('before', num);
								});
						});

						this.$afterBtn.on('mousedown touchstart', function (e) {

								e.preventDefault();

								// 後面按鈕處置.
								_this.$ele.on('mousemove touchmove', function (e) {

										e.preventDefault();

										// mouse X.
										var num = e.clientX || e.originalEvent.touches[0].pageX;

										// 設定按鈕位置.
										_this.setBtnPosition('after', num);
								});
						});

						this.removeEvent();
				}
		}, {
				key: 'setMouseClick',
				value: function setMouseClick() {
						var _this2 = this;

						this.$ele.on('mousedown touchstart', function (e) {

								e.preventDefault();

								var num = e.clientX || e.originalEvent.touches[0].pageX;
								var nearestBtnName = _this2.checkNearestBtn(num);

								_this2.setBtnPosition(nearestBtnName, num);

								_this2.$ele.on('mousemove touchmove', function (e) {

										e.preventDefault();

										// mouse X.
										var tempNum = e.clientX || e.originalEvent.touches[0].pageX;

										// 設定按鈕位置.
										_this2.setBtnPosition(nearestBtnName, tempNum);
								});
						});
				}
		}, {
				key: 'removeEvent',
				value: function removeEvent() {
						var _this3 = this;

						this.$ele.on('mouseup touchend', function (e) {

								e.preventDefault();
								_this3.$ele.off('mousemove touchmove');
								_this3.$beforeBtn.off('mouseover');
								_this3.$afterBtn.off('mouseover');
								_this3.$beforeBtn.children('span').removeClass('active');
								_this3.$afterBtn.children('span').removeClass('active');
						});

						this.$ele.on('mouseleave touchend', function (e) {

								e.preventDefault();
								_this3.$ele.off('mousemove touchmove');
								_this3.$beforeBtn.off('mouseover');
								_this3.$afterBtn.off('mouseover');
								_this3.$beforeBtn.children('span').removeClass('active');
								_this3.$afterBtn.children('span').removeClass('active');
						});
				}
		}, {
				key: 'setRangeInit',
				value: function setRangeInit(reInitSwitch) {

						if (reInitSwitch == 'reInit') {

								// 執行reInit時.
								if (this.mode == 'time') {

										this.$beforeBtn.offset({ left: this.$greyBar.offset().left + this.convertPercentageToNum(this.convertTimeToPecentage(this.valueArr[0])) - 10 });
										this.$afterBtn.offset({ left: this.$greyBar.offset().left + this.convertPercentageToNum(this.convertTimeToPecentage(this.valueArr[1])) - 10 });
								} else {

										this.$beforeBtn.offset({ left: this.$greyBar.offset().left + this.convertPercentageToNum(this.valueArr[0]) - 10 });
										this.$afterBtn.offset({ left: this.$greyBar.offset().left + this.convertPercentageToNum(this.valueArr[1]) - 10 });
								}
						} else {

								// 執行init時.
								// 設定bar位置method (限定拖拉範圍).
								var tempMin = this.convertPercentageToNum(this.min);
								var tempMax = this.convertPercentageToNum(this.max);

								this.$beforeBtn.offset({ left: this.$greyBar.offset().left + tempMin - 10 });
								this.$afterBtn.offset({ left: this.$greyBar.offset().left + tempMax - 10 });
						}

						// redBar的縮放.
						this.setRedBarW();
				}
		}, {
				key: 'setBtnFollowMouse',
				value: function setBtnFollowMouse($currentBtn, num) {

						$currentBtn.offset({ left: num - 10 });
				}
		}, {
				key: 'setRedBarW',
				value: function setRedBarW() {
						this.$redBar.outerWidth(this.$afterBtn.position().left - this.$beforeBtn.position().left + 10);
						this.$redBar.offset({ left: this.$beforeBtn.offset().left + 10 });
				}
		}, {
				key: 'setValue',
				value: function setValue(arr) {

						// this.isSetValue = true;
						this.setValueTempArr = arr;

						this.setMinMaxInit();

						var insertMin = 0;
						var insertMax = 100;
						var tempMin = this.convertPercentageToNum(this.min); // 最小範圍
						var tempMax = this.convertPercentageToNum(this.max); // 最大範圍

						// 設定bar位置method.
						if (this.mode == 'time') {
								insertMin = this.convertPercentageToNum(this.convertTimeToPecentage(arr[0]));
								insertMax = this.convertPercentageToNum(this.convertTimeToPecentage(arr[1]));
						} else {
								insertMin = this.convertPercentageToNum(arr[0]);
								insertMax = this.convertPercentageToNum(arr[1]);
						}

						// 限制區域
						if (insertMin < tempMin) insertMin = tempMin;else this.valueArr[0] = arr[0];

						if (insertMax > tempMax) insertMax = tempMax;else this.valueArr[1] = arr[1];

						this.$beforeBtn.offset({ left: this.$greyBar.offset().left + insertMin - 10 });
						this.$afterBtn.offset({ left: this.$greyBar.offset().left + insertMax - 10 });

						// redBar的縮放.
						this.setRedBarW();

						// 觸發change事件.
						this.changeCallBack(this.valueArr);
				}
		}, {
				key: 'setBarDisable',
				value: function setBarDisable() {

						// disable.
						this.$beforeBtn.off('mousedown');
						this.$afterBtn.off('mousedown');
						this.$ele.off('mousemove');
						this.$ele.off('mouseup');
						this.$ele.off('mouseleave');
				}
		}, {
				key: 'resizer',
				value: function resizer() {
						var _this4 = this;

						// window resize事件.
						$(window).on('resize', function () {
								_this4.reInit();
						});
				}
		}, {
				key: 'reInit',
				value: function reInit() {

						this.greyBarW = this.$greyBar.outerWidth();

						// 設定range.
						this.setRangeInit('reInit');

						// 觸發change事件.
						this.changeCallBack(this.valueArr);
				}
		}, {
				key: 'init',
				value: function init() {

						this.$ele.addClass(ModuleName);
						this.$ele.append(this.barWrap);

						// before btn.
						this.$beforeBtn = this.$ele.find('.slider_btn_before');

						// after btn.
						this.$afterBtn = this.$ele.find('.slider_btn_after');

						// redBar.
						this.$redBar = this.$ele.find('.slider_range_red');

						// greyBar.
						this.$greyBar = this.$ele.find('.slider_range_grey');

						this.greyBarW = this.$greyBar.outerWidth();

						// 設定最大最小值.
						this.setMinMaxInit();

						// 設定range.
						this.setRangeInit();

						// resize事件.
						this.resizer();

						if (this.disable) {
								this.$ele.addClass('disable');
								return;
						}

						// 設定mouse事件.
						this.setMouseDown();
						this.setMouseClick();

						// 觸發change事件.
						this.changeCallBack(this.valueArr);
				}
		}]);

		return Module;
}();

;

exports.ModuleName = ModuleName;
exports.ModuleDefaults = ModuleDefaults;
exports.ModuleReturns = ModuleReturns;
exports.Module = Module;

/***/ })
/******/ ]);
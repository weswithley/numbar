Numbar
a number / time format range slider bar.

module option :  
min : Minimum value. (string or number)(ex: "00:10" or 0)
max : Maximum value. (string or number)(ex: "24:00" or 100)
disable : disabled status. (boolean)(ex: true or false)  
mode: mode status. (string)("time" or "number", "number" is default.).   
change : on cnage callback 
        (function)
        (default params would passing into this callback function is a 2 length array, 
        array[0] means left bar current value, 
        array[1] means right bar current value. )  
setValues : a method function for setting slider value & bar position (array)(ex: [10, 25])  
  
usage : 
```
initialize:
$( ".selector" ).numbar({  
    min: 0,  
    max: 100,  
    disable: false,  
    mode: 'time',  
    change: function(numArr){  
        console.log('left bar value: ', numArr[0]);  
        console.log('right bar value: ', numArr[1]);  
    }  
});  
  
set slider & bar value:  
$( ".selector" ).numbar('setValues', [10, 25]);  
```


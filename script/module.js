const ModuleName = 'numbar';
const ModuleDefaults =  {
	min : null, // string / number.
	max : null, // string / number.
	disable : false, // boolean.
	mode : 'number', // string.
	reInit : () => {}, // function.
	setValues : () => {}, // function.
	change : () => {} // function.
};
const ModuleReturns = ['output', 'methods'];

class Module {
	constructor ( ele, options ) {
		this.ele = ele;
		this.$ele = $(ele);
		this.option = options;
		this.barWrap = `<div class="slider_range_wrap">
							<div class="slider_btn_before slider_btn"><span></span></div>
							<div class="slider_btn_after slider_btn"><span></span></div>
							<div class="slider_range_red slider_bg"></div>
							<div class="slider_range_grey slider_bg"></div>
						</div>`;
		this.$beforeBtn;

		this.$afterBtn;

		this.$greyBar;
		this.greyBarW = 0;

		this.$redBar;

		this.valueArr = [];

		this.disable = this.option.disable;

		this.changeCallBack = this.option.change;
		
		this.mode = this.option.mode || ModuleDefaults.mode;

		this.min = 0;
		this.max = 100;

		// this.isSetValue = false;
		this.setValueTempArr = [];

		this.mouseClickXnum = 0;

		this.currentBtn; // 目前點擊的btn.

	}
	convertNumToPercentage(percentNum){
		// 把輸入number, 轉為greyBarW的百分比.
		let percentValue = Math.floor( percentNum / this.greyBarW * 100 );
		return percentValue;
	}
	convertPercentageToNum(percentNum){
		// 把輸入百分比, 轉為greyBarW的百分比.
		if(percentNum > 100){
			percentNum = 100;
		}else if(percentNum < 0){
			percentNum = 0;
		}
		let numValue = this.greyBarW * (percentNum / 100);
		
		return numValue;
	}
	convertTimeToPecentage(timeString){

		// 把輸入時間字串, 轉為greyBarW的百分比.
		let timeStringSplitArr = timeString.split(':');
		// 時
		let hourValue = Number(timeStringSplitArr[0]) * 60;
		// 分
		let minsValue = 0; //分不用做運算處理, 所以直接為0.
		let totalValue = Math.floor((hourValue + minsValue) / 1440 * 100);

		if(totalValue > 100){
			totalValue = 100;
		}else if(totalValue < 0){
			totalValue = 0;
		}

		return totalValue;
	}
	convertPecentageToTime(percentNum){
		// 把輸入的百分比, 轉為時間字串.
		let percentValue = Math.round( 1440 * (percentNum / 100) / 60 ); // 改為四捨五入.
		let percentString = '';
		if(percentValue >= 10){
			percentString = percentValue.toString() + ':00';
		}else{
			percentString = '0' + percentValue.toString() + ':00';
		}
		return percentString;
	}
	changeCallBack(arr){
		// 數值change時的callback.
	}
	setMinMaxInit(){
		if( this.option.min ){
			if( typeof this.option.min == 'string' || this.mode =='time' ){
				// 為時間模式
				this.min = this.convertTimeToPecentage(this.option.min);
				this.valueArr[0] = this.option.min;
			}else{
				// 為數字模式
				this.min = this.option.min;
				this.valueArr[0] = this.option.min;
			}
		}else{
			if( this.mode =='time'){
				// 為時間模式
				this.valueArr[0] = this.convertPecentageToTime(this.min);
			}else{
				// 為數字模式
				this.valueArr[0] = this.min;
			}
		}

		if( this.option.max ){
			if( typeof this.option.max == 'string' || this.mode =='time' ){
				// 為時間模式
				this.max = this.convertTimeToPecentage(this.option.max);
				this.valueArr[1] = this.option.max;
			}else{
				// 為數字模式
				this.max = this.option.max;
				this.valueArr[1] = this.option.max;
			}
		}else{
			if( this.mode =='time'){
				// 為時間模式
				this.valueArr[1] = this.convertPecentageToTime(this.max);
			}else{
				// 為數字模式
				this.valueArr[1] = this.max = this.max;
			}
		}
	}
	checkNearestBtn(num){

		let mouseToBeforeBtn = num - (this.$beforeBtn.offset().left + 10);
		if(mouseToBeforeBtn < 0) mouseToBeforeBtn = mouseToBeforeBtn * -1;

		let mouseToAfterBtn = (this.$afterBtn.offset().left + 10) - num;
		if(mouseToAfterBtn < 0) mouseToAfterBtn = mouseToAfterBtn * -1;

		if( mouseToBeforeBtn < mouseToAfterBtn ){
			// 靠近before btn.
			return 'before';
		}else if( mouseToBeforeBtn > mouseToAfterBtn ){
			// 靠近after btn.
			return 'after';
		}else{

			if(this.currentBtn == this.$beforeBtn){
				return 'before';
			}else if(this.currentBtn == this.$afterBtn){
				return 'after';
			}else{
				console.log( 'checkNearestBtn get error btn : ' +  this.currentBtn );
			}

		}

	}
	setBtnPosition(typeOfBtn, num){

		if(typeOfBtn == 'before'){

			// before.
			let tempMin = this.convertPercentageToNum(this.min);

			//set btn z-index;
			this.$beforeBtn.css('z-index', 9);
			this.$afterBtn.css('z-index', 5);

			this.currentBtn = this.$beforeBtn;

			//before btn.
			this.$beforeBtn.children('span').addClass('active');

			// 限制區域
			if(num < this.$greyBar.offset().left + tempMin) num = this.$greyBar.offset().left + tempMin;
			else if(num > this.$afterBtn.offset().left + 10) num = this.$afterBtn.offset().left + 10;

			// 按鈕跟隨滑鼠.
			this.setBtnFollowMouse( this.$beforeBtn, num);

			if(this.mode == 'time'){
				this.valueArr[0] = this.convertPecentageToTime( this.convertNumToPercentage(num - this.$greyBar.offset().left) );
			}else{
				this.valueArr[0] = this.convertNumToPercentage(num - this.$greyBar.offset().left);
			}

			// redBar的縮放
			this.setRedBarW();

			// 把目前變換的value array存進callback.
			this.changeCallBack(this.valueArr);

		}else if(typeOfBtn == 'after'){

			// after.
			let tempMax = this.convertPercentageToNum(this.max);

			//set btn z-index;
			this.$beforeBtn.css('z-index', 5);
			this.$afterBtn.css('z-index', 9);

			this.currentBtn = this.$afterBtn;

			//after btn.
			this.$afterBtn.children('span').addClass('active');
				
			// 限制區域
			if(num > this.$greyBar.offset().left + tempMax) num = this.$greyBar.offset().left + tempMax;
			else if(num < this.$beforeBtn.offset().left + 10) num = this.$beforeBtn.offset().left + 10;

			// 按鈕跟隨滑鼠.
			this.setBtnFollowMouse( this.$afterBtn, num);

			if(this.mode == 'time'){
				this.valueArr[1] = this.convertPecentageToTime( this.convertNumToPercentage(num - this.$greyBar.offset().left) );
			}else{
				this.valueArr[1] = this.convertNumToPercentage(num - this.$greyBar.offset().left);
			}

			// redBar的縮放
			this.setRedBarW();

			// 把目前變換的value array存進callback.
			this.changeCallBack(this.valueArr);

		}else{
			console.log( 'setBtnPosition()傳入參數格式錯誤 : ' + typeOfBtn );
		}

	}
	setMouseDown () {

		this.$beforeBtn.on('mousedown touchstart', (e) => {

			e.preventDefault();

			// 前面按鈕處置.
			this.$ele.on('mousemove touchmove', (e) => {

				e.preventDefault();

				// mouse X.
				let num = e.clientX || e.originalEvent.touches[0].pageX;

				// 設定按鈕位置.
				this.setBtnPosition( 'before' , num);

			});

		})

		this.$afterBtn.on('mousedown touchstart', (e) => {

			e.preventDefault();

			// 後面按鈕處置.
			this.$ele.on('mousemove touchmove', (e) => {

				e.preventDefault();

				// mouse X.
				let num = e.clientX || e.originalEvent.touches[0].pageX;

				// 設定按鈕位置.
				this.setBtnPosition( 'after' , num);

			});

		})

		this.removeEvent();

	}
	setMouseClick(){

		this.$ele.on('mousedown touchstart', (e) => {

			e.preventDefault();

			let num = e.clientX || e.originalEvent.touches[0].pageX;
			let nearestBtnName = this.checkNearestBtn(num);

			this.setBtnPosition( nearestBtnName , num);

			this.$ele.on('mousemove touchmove', (e) => {

				e.preventDefault();

				// mouse X.
				let tempNum = e.clientX || e.originalEvent.touches[0].pageX;

				// 設定按鈕位置.
				this.setBtnPosition( nearestBtnName , tempNum);

			});

		})
		
	}
	removeEvent(){

		this.$ele.on('mouseup touchend', (e) => {

			e.preventDefault();
			this.$ele.off('mousemove touchmove');
			this.$beforeBtn.off('mouseover');
			this.$afterBtn.off('mouseover');
			this.$beforeBtn.children('span').removeClass('active');
			this.$afterBtn.children('span').removeClass('active');

		})

		this.$ele.on('mouseleave touchend', (e) => {

			e.preventDefault();
			this.$ele.off('mousemove touchmove');
			this.$beforeBtn.off('mouseover');
			this.$afterBtn.off('mouseover');
			this.$beforeBtn.children('span').removeClass('active');
			this.$afterBtn.children('span').removeClass('active');
			
		})

	}
	setRangeInit(reInitSwitch){

		if(reInitSwitch == 'reInit'){

			// 執行reInit時.
			if(this.mode == 'time'){
			
				this.$beforeBtn.offset({ left: this.$greyBar.offset().left + this.convertPercentageToNum( this.convertTimeToPecentage( this.valueArr[0] ) ) - 10 });
				this.$afterBtn.offset({ left: this.$greyBar.offset().left + this.convertPercentageToNum( this.convertTimeToPecentage( this.valueArr[1] ) ) - 10 });
			
			}else{

				this.$beforeBtn.offset({ left: this.$greyBar.offset().left + this.convertPercentageToNum( this.valueArr[0] ) - 10 });
				this.$afterBtn.offset({ left: this.$greyBar.offset().left + this.convertPercentageToNum( this.valueArr[1] ) - 10 });
			}

		}else{

			// 執行init時.
			// 設定bar位置method (限定拖拉範圍).
			let tempMin = this.convertPercentageToNum(this.min);
			let tempMax = this.convertPercentageToNum(this.max);

			this.$beforeBtn.offset({ left: this.$greyBar.offset().left + tempMin - 10});
			this.$afterBtn.offset({ left: this.$greyBar.offset().left + tempMax - 10});

		}

		// redBar的縮放.
		this.setRedBarW();

	}
	setBtnFollowMouse($currentBtn, num){

		$currentBtn.offset({ left:num - 10 });

	}
	setRedBarW(){
		this.$redBar.outerWidth( this.$afterBtn.position().left - this.$beforeBtn.position().left + 10 );
		this.$redBar.offset({ left: this.$beforeBtn.offset().left + 10 });
	}
	setValue(arr){

		// this.isSetValue = true;
		this.setValueTempArr = arr;

		this.setMinMaxInit();

		let insertMin = 0;
		let insertMax = 100;
		let tempMin = this.convertPercentageToNum(this.min); // 最小範圍
		let tempMax = this.convertPercentageToNum(this.max); // 最大範圍

		// 設定bar位置method.
		if(this.mode == 'time'){
			insertMin = this.convertPercentageToNum( this.convertTimeToPecentage(arr[0]) );
			insertMax = this.convertPercentageToNum( this.convertTimeToPecentage(arr[1]) );
			
		}else{
			insertMin = this.convertPercentageToNum( arr[0] );
			insertMax = this.convertPercentageToNum( arr[1] );
		}

		// 限制區域
		if(insertMin < tempMin) insertMin = tempMin;
		else this.valueArr[0] = arr[0];

		if(insertMax > tempMax) insertMax = tempMax;
		else this.valueArr[1] = arr[1];

		this.$beforeBtn.offset({ left: this.$greyBar.offset().left + insertMin - 10});
		this.$afterBtn.offset({ left: this.$greyBar.offset().left + insertMax - 10});

		// redBar的縮放.
		this.setRedBarW();

		// 觸發change事件.
		this.changeCallBack(this.valueArr);

	}
	setBarDisable () {

		// disable.
		this.$beforeBtn.off('mousedown');
		this.$afterBtn.off('mousedown');
		this.$ele.off('mousemove');
		this.$ele.off('mouseup');
		this.$ele.off('mouseleave');

	}
	resizer(){

		// window resize事件.
		$(window).on('resize', () => {
			this.reInit();
		})

	}
	reInit(){

		this.greyBarW = this.$greyBar.outerWidth();

		// 設定range.
		this.setRangeInit('reInit');

		// 觸發change事件.
		this.changeCallBack(this.valueArr);

	}
	init(){

		this.$ele.addClass(ModuleName);
		this.$ele.append(this.barWrap);

		// before btn.
		this.$beforeBtn = this.$ele.find('.slider_btn_before');

		// after btn.
		this.$afterBtn = this.$ele.find('.slider_btn_after');

		// redBar.
		this.$redBar = this.$ele.find('.slider_range_red');

		// greyBar.
		this.$greyBar = this.$ele.find('.slider_range_grey');

		this.greyBarW = this.$greyBar.outerWidth();

		// 設定最大最小值.
		this.setMinMaxInit();

		// 設定range.
		this.setRangeInit();

		// resize事件.
		this.resizer();

		if(this.disable){
			this.$ele.addClass('disable');
			return;
		}

		// 設定mouse事件.
		this.setMouseDown();
		this.setMouseClick();

		// 觸發change事件.
		this.changeCallBack(this.valueArr);

	}
};

export { ModuleName, ModuleDefaults, ModuleReturns, Module };